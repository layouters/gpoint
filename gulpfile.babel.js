var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var webserver = require('gulp-webserver');
var gutil = require('gulp-util');
var ftp = require('gulp-ftp');
var combine_mq = require('gulp-combine-mq');
var postcss = require('gulp-postcss');
var cssnano = require('cssnano');
var colorRgbaFallback = require("postcss-color-rgba-fallback");
var willChange = require("postcss-will-change");
var html5Lint = require('gulp-html5-lint');
var htmlMinify = require('gulp-minify-html');
var imagemin = require('gulp-imagemin');
var fixJs = require("gulp-fixmyjs");
var streamify = require('gulp-streamify');
var packer = require('gulp-packer');
var rename = require('gulp-rename');
var htmlReplace = require('gulp-html-replace');

var config = {
    source: './src/',
    dev: './dev/',
    deploy: './deploy/',
    html: {
        include: '*.html',
        exclude: '',
        destination: ''
    },
    json: {
        include: '**/*.json',
        exclude: '',
        destination: ''
    },
    sass: {
        include: 'sass/**/*.scss',
        exclude: 'sass/**/*.ignore.scss',
        destination: 'css'
    },
    js: {
        include: 'js/main.js',
        pluginIndex: 'plugins.js',
        plugins: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js'
        ],
        destination: 'js'
    },
    fonts: {
        include: 'fonts/**/*',
        destination: 'fonts'
    },
    images: {
        include: 'img/**/*.{jpg,png,svg}',
        destination: 'img'
    }
};

gulp.task('sass', function () {
    return gulp.src([config.source + config.sass.include, '!' + config.source + config.sass.exclude])
        .pipe(sourcemaps.init())
        .pipe(sass())
        //.pipe(combine_mq())
        .pipe(postcss([
            colorRgbaFallback(),
            willChange(),
            autoprefixer()
        ]))
        .pipe(sourcemaps.write({
            includeContent: true
        }))
        .pipe(gulp.dest(config.dev + config.sass.destination));
});
gulp.task('sass:deploy', function () {
    return gulp.src([config.source + config.sass.include, '!' + config.source + config.sass.exclude])
        .pipe(sass())
        .pipe(combine_mq())
        .pipe(postcss([
            colorRgbaFallback(),
            willChange(),
            autoprefixer(),
            cssnano()
        ]))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest(config.deploy + config.sass.destination));
});

gulp.task('json', function() {
    return gulp.src([config.source + config.json.include, '!' + config.source + config.json.exclude])
        .pipe(gulp.dest(config.dev + config.json.destination));
});
gulp.task('json:deploy', function() {
    return gulp.src([config.source + config.json.include, '!' + config.source + config.json.exclude])
        .pipe(gulp.dest(config.deploy + config.json.destination));
});

gulp.task('html', ['json'], function() {
    return gulp.src([config.source + config.html.include, '!' + config.source + config.html.exclude])
        //.pipe(html5Lint())
        .pipe(gulp.dest(config.dev + config.html.destination));
});
gulp.task('html:deploy', ['json:deploy'], function() {
    return gulp.src([config.source + config.html.include, '!' + config.source + config.html.exclude])
        .pipe(htmlReplace({
            //'fonts'		: 'css/fonts.min.css',
            'css'           : 'css/main.min.css',
            'js-plugins'    : 'js/plugins.min.js',
            'js-main'       : 'js/main.min.js'
        }))
        .pipe(htmlMinify())
        .pipe(gulp.dest(config.deploy + config.html.destination));
});

gulp.task('img', function() {
    return gulp.src(config.source + config.images.include)
        .pipe(gulp.dest(config.dev + config.images.destination));
});
gulp.task('img:deploy', function() {
    return gulp.src(config.source + config.images.include)
        .pipe(imagemin())
        .pipe(gulp.dest(config.deploy + config.images.destination));
});

 gulp.task('fonts', function () {
     return gulp.src(config.source + config.fonts.include)
         .pipe(gulp.dest(config.dev + config.fonts.destination));
 });
 gulp.task('fonts:deploy', function () {
     return gulp.src(config.source + config.fonts.include)
         .pipe(gulp.dest(config.deploy + config.fonts.destination));
 });

gulp.task('js-plugins', function() {
    return gulp.src(config.js.plugins)
        .pipe(concat(config.js.pluginIndex))
        .pipe(gulp.dest(config.dev + config.js.destination));
});
gulp.task('js-plugins:deploy', function() {
    return gulp.src(config.js.plugins)
        .pipe(fixJs())
        .pipe(concat(config.js.pluginIndex))
        .pipe(streamify(packer({
            base62: true,
            shrink: true
        })))
        .pipe(gulp.dest(config.deploy + config.js.destination));
});

gulp.task('js', ['js-plugins'], function() {
    return gulp.src(config.source + config.js.include)
        .pipe(gulp.dest(config.dev + config.js.destination));
});
gulp.task('js:deploy', ['js-plugins:deploy'], function() {
    return gulp.src(config.source + config.js.include)
        .pipe(fixJs())
        .pipe(streamify(packer({
            base62: true,
            shrink: true
        })))
        .pipe(gulp.dest(config.deploy + config.js.destination));
});

gulp.task('webserver', function() {
    gulp.src(config.dev)
        .pipe(webserver());
});
gulp.task('webserver:deploy', function() {
    gulp.src(config.deploy)
        .pipe(webserver());
});

gulp.task('ftp', function() {
    return gulp.src(config.dev + '**/*')
        .pipe(ftp({
            host: '95.213.179.227',
            user: 'shleyzec_gpoint',
            pass: '8S7x5Y3r'
        }))
        .pipe(gutil.noop());
});
gulp.task('ftp:deploy', function() {
    return gulp.src(config.deploy + '**/*')
        .pipe(ftp({
            host: '95.213.179.227',
            user: 'shleyzec_gpoint',
            pass: '8S7x5Y3r'
        }))
        .pipe(gutil.noop());
});

gulp.task('watch', function() {
    gulp.watch(config.source + config.html.include, ['html']);
    gulp.watch(config.source + config.json.include, ['json']);
    gulp.watch(config.source + config.sass.include, ['sass']);
    gulp.watch(config.source + config.images.include, ['img']);
    gulp.watch(config.source + config.js.include, ['js']);
});

gulp.task('deploy', ['fonts:deploy', 'html:deploy', 'sass:deploy', 'js:deploy', 'img:deploy']);
gulp.task('default', ['fonts', 'html', 'sass', 'js', 'img']);