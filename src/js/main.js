$(function () {
    // Мобильное меню
    var $menu = $('.page-header__menu'),
        $triggers = $('.js-menu-trigger'),
        menuActiveClass = 'page-header__menu--active';

    $triggers.on('click', function () {
        $menu.toggleClass(menuActiveClass)
    });

    // Дропдаун для расписания nav-calendar__dropdown
    var $navCalendar = $('.nav-calendar'),
        $navCalendarListItem = $navCalendar.find('li'),
        $navDropDown = $('.nav-calendar__dropdown .dropdown'),
        $navDropDownMenu = $navDropDown.find('.dropdown-menu'),
        $navDropDownMenuListItem = $navDropDownMenu.find('li'),
        $navToggle = $navDropDown.find('.dropdown-toggle'),
        activeClass = 'active',
        dateClass = 'js-nav-calendar-date',
        dayClass = 'js-nav-calendar-day',
        $navToggleDate = $navToggle.find('.' + dateClass),
        $navToggleDay = $navToggle.find('.' + dayClass),
        $navCalendarAndDropDown = $navCalendar.add($navDropDown);


    $navCalendarAndDropDown.on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
        var $activeLink = $(e.target),
            $activeListItem = $activeLink.closest('li'),
            activeItemIndex = $activeListItem.index();

        // При клике на табы
        $navCalendarListItem.eq(activeItemIndex).addClass(activeClass).siblings().removeClass(activeClass);

        // При клике на дропдаун
        $navDropDownMenuListItem.eq(activeItemIndex).addClass(activeClass).siblings().removeClass(activeClass);
        $navToggleDate.html($navDropDownMenuListItem.eq(activeItemIndex).find('.' + dateClass).html());
        $navToggleDay.html($navDropDownMenuListItem.eq(activeItemIndex).find('.' + dayClass).html());
    })

});